package-lock-lint
=================

A tool to lint npm's package-lock.json files at a basic level since they're
impossible to review manually.

```
$ package-lock-lint /my/package-lock.json
```

Current checks:
* Matches overall schema
* Dependencies resolve to valid URLs (catches [T278857](https://phabricator.wikimedia.org/T278857))
* Dependencies are downloaded over secure channels (HTTPS or SSH)
* Package `-` is not depended upon ([typo](https://www.bleepingcomputer.com/news/software/empty-npm-package-has-over-700-000-downloads-heres-why/))

See [T242058: Add some form of static analysis for package-lock.json](https://phabricator.wikimedia.org/T242058)
for discussion and inspiration that let to this tool.

(C) 2021 Kunal Mehta, under the GPL v3 or any later version.
