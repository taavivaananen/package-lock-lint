/**
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use serde::Deserialize;
use std::collections::HashMap;
use std::path::Path;
use std::{env, fs, process};
use url::Url;

/// Structure that represents v1, v2 and v3 lockfiles
#[derive(Deserialize)]
struct PackageLock {
    // requires: bool,
    #[serde(rename = "lockfileVersion")]
    lockfile_version: u32,
    /// in lockfileVersion 1, dependencies are recursively nested under
    /// "dependencies". Note that all dependencies here match `Dependency` and
    /// not `Current`, but it's just easier to have them all be the same type.
    #[serde(default)]
    dependencies: HashMap<String, Package>,
    /// in lockfileVersion 2, dependencies are flatly listed under "packages".
    #[serde(default)]
    packages: HashMap<String, Package>,
}

/// Handle the "" package that contains the current project's dependencies,
/// but doesn't fit the Dependency schema. Sigh.
#[derive(Deserialize)]
#[serde(untagged)]
enum Package {
    Current(CurrentProject),
    Dependency(Dependency),
}

/// In V2, the first package is actually the current project and just contains
/// the project's metadata. In practice, there should always be some
/// devDependencies and that key will not be in the rest of the packages.
/// TODO: can we just ignore "" instead of this?
#[derive(Deserialize)]
struct CurrentProject {
    #[serde(rename = "devDependencies")]
    _dev_dependencies: HashMap<String, String>,
}

/// Check the entire lockfile
fn check_lock(lock: &PackageLock) -> Vec<String> {
    let pkgs = if lock.lockfile_version >= 2 {
        &lock.packages
    } else {
        &lock.dependencies
    };
    pkgs.iter()
        .flat_map(|(name, pkg)| {
            match pkg {
                Package::Current(_) => {
                    // Don't care, skip
                    vec![]
                }
                Package::Dependency(dep) => check_dep(name, dep),
            }
        })
        .collect()
}

/// The "dependencies" field has two different
/// formats in lockfileVersion 1 and 2. In v1,
/// it's a recursive `Dependency` struct. In
/// `v2` it's just a version string.
#[derive(Deserialize)]
#[serde(untagged)]
enum InnerDependency {
    V2(String),
    V1(Dependency),
}

#[derive(Deserialize)]
struct Dependency {
    version: Option<String>,
    // from: Option<String>,
    resolved: Option<String>,
    // integrity: Option<String>,
    // #[serde(default)]
    // dev: bool,
    // #[serde(default)]
    // requires: HashMap<String, String>,
    #[serde(default)]
    dependencies: HashMap<String, InnerDependency>,
}

/// Check a dependency, possibly recursively.
fn check_dep(name: &str, dep: &Dependency) -> Vec<String> {
    let mut resp = vec![];

    if let Some(msg) = validate(name, dep) {
        resp.push(format!(
            "{}@{}: {}",
            name,
            dep.version.clone().unwrap_or_else(|| "unknown".to_string()),
            msg
        ));
    }

    for (name, inner) in dep.dependencies.iter() {
        match inner {
            InnerDependency::V2(_) => {
                // Do nothing, no need to recurse
            }
            InnerDependency::V1(inner_dep) => {
                resp.extend(check_dep(name, inner_dep));
            }
        };
    }

    resp
}

/// Perform validation against a dependency, possibly returning an error
/// message.
fn validate(name: &str, dep: &Dependency) -> Option<String> {
    // Check for a package named "-"
    // https://www.bleepingcomputer.com/news/software/empty-npm-package-has-over-700-000-downloads-heres-why/
    if name == "-" {
        return Some("Package \"-\" found".to_string());
    }

    // One of resolved or version must be present
    if dep.resolved.is_none() && dep.version.is_none() {
        return Some("Neither \"resolved\" nor \"version\" are present".to_string());
    }

    // If resolved is present, we check it first and rely on it
    if let Some(resolved) = &dep.resolved {
        match Url::parse(resolved) {
            Ok(url) => {
                if url.scheme() == "file" {
                    // Part of repository, OK (e.g. wdio-mediawiki in MediaWiki core)
                    // This uses strip_prefix("file:") because url.path() will always have
                    // a heading slash (for an absolute path) inserted due to some stupid reason.
                    if !validate_local_path(resolved.strip_prefix("file:").unwrap()) {
                        return Some(format!("\"resolved\" is an invalid local path: {}", resolved));
                    }
                    return None;
                }
                if !is_secure_proto(url.scheme()) {
                    return Some(format!("\"resolved\" does not use HTTPS: {}", resolved));
                }
            }
            Err(e) => {
                return Some(format!(
                    "\"resolved\" is not a valid URL: \"{}\" ({})",
                    resolved, e
                ));
            }
        };
        // resolved is OK, so this whole dep is OK.
        return None;
    }

    // version might be a valid URL
    if let Some(version) = &dep.version {
        if let Ok(url) = Url::parse(version) {
            if url.scheme() == "file" {
                // Part of repository, OK (e.g. wdio-mediawiki in MediaWiki core)
                // This uses strip_prefix("file:") because url.path() will always have
                // a heading slash (for an absolute path) inserted due to some stupid reason.
                if !validate_local_path(version.strip_prefix("file:").unwrap()) {
                    return Some(format!("\"version\" is an invalid local path: {}", version));
                }
                return None;
            }
            if !is_secure_proto(url.scheme()) {
                return Some(format!("\"version\" does not use HTTPS: {}", url));
            }
        }
    }
    None
}

/// Whether the provided protocol is secure
fn is_secure_proto(proto: &str) -> bool {
    // git+ssh is bad but it's not insecure
    ["git+https", "git+ssh", "https", "github"].contains(&proto)
}

fn validate_local_path(path: &str) -> bool {
    return Path::new(path).is_dir()
}

fn main() {
    let mut found_errors = false;
    for (index, path) in env::args().enumerate() {
        if index == 0 {
            continue;
        }
        println!("Checking {}", &path);
        let text = match fs::read_to_string(&path) {
            Ok(text) => text,
            Err(err) => {
                println!("Error reading file: {}", err);
                found_errors = true;
                continue;
            }
        };
        let json: PackageLock = match serde_json::from_str(&text) {
            Ok(json) => json,
            Err(err) => {
                println!("Error parsing JSON: {}", err);
                found_errors = true;
                continue;
            }
        };
        // We only support lockfileVersion 1, 2 and 3
        if json.lockfile_version > 3 {
            println!(
                "Unspported lockfileVersion ({}): {}",
                json.lockfile_version, &path
            );
            found_errors = true;
            continue;
        }
        let issues = check_lock(&json);
        if !issues.is_empty() {
            println!("{}", issues.join("\n"));
            found_errors = true;
        }
    }
    if found_errors {
        process::exit(1);
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_validate() {
        let mut dep = Dependency {
            version: Some("1.0".to_string()),
            resolved: None,
            dependencies: Default::default(),
        };
        // No errors
        assert!(validate("pkg", &dep).is_none());
        // We rely on /etc being a directory here
        dep.resolved = Some("file:/etc".to_string());
        assert!(validate("pkg", &dep).is_none());

        assert_eq!(validate("-", &dep).unwrap(), "Package \"-\" found");
        dep.resolved = Some("not a valid URL".to_string());
        assert!(validate("pkg", &dep)
            .unwrap()
            .starts_with("\"resolved\" is not a valid URL"));
        dep.resolved = Some("http://registry.example.org".to_string());
        assert!(validate("pkg", &dep)
            .unwrap()
            .starts_with("\"resolved\" does not use HTTPS"));
        dep.resolved = Some("file:/nonexistent".to_string());
        assert!(validate("pkg", &dep)
            .unwrap()
            .starts_with("\"resolved\" is an invalid local path"));
        dep.resolved = None;

        // No errors
        dep.version = Some("file:/etc".to_string());
        assert!(validate("pkg", &dep).is_none());

        dep.version = Some("http://git.example.org/cool-package.git".to_string());
        assert!(validate("pkg", &dep)
            .unwrap()
            .starts_with("\"version\" does not use HTTPS"));
        dep.version = Some("file:/nonexistent".to_string());
        assert!(validate("pkg", &dep)
            .unwrap()
            .starts_with("\"version\" is an invalid local path"));

        dep.version = None;
        dep.resolved = None;
        assert_eq!(
            validate("pkg", &dep).unwrap(),
            "Neither \"resolved\" nor \"version\" are present"
        );
    }

    #[test]
    fn test_is_secure_proto() {
        assert!(is_secure_proto("https"));
        assert!(is_secure_proto("git+ssh"));
        assert!(!is_secure_proto("http"));
        assert!(!is_secure_proto("git"));
    }
}
